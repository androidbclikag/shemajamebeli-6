package com.example.quiz6

class ImageModel {
    var image:String? = ""
    val imageArray:ArrayList<String> = arrayListOf(albumOne,albumTwo,albumThree, albumFour, albumFive)

}

val albumOne = "https://upload.wikimedia.org/wikipedia/en/0/03/Queen_Queen.png"
val albumTwo =
    "https://upload.wikimedia.org/wikipedia/en/thumb/4/4d/Queen_A_Night_At_The_Opera.png/220px-Queen_A_Night_At_The_Opera.png"
val albumThree =
    "https://upload.wikimedia.org/wikipedia/en/thumb/f/f7/Queen_Innuendo.png/220px-Queen_Innuendo.png"
val albumFour =
    "https://upload.wikimedia.org/wikipedia/ka/thumb/a/ad/Queen_II.jpg/230px-Queen_II.jpg"
val albumFive =
    "https://upload.wikimedia.org/wikipedia/en/thumb/0/06/Queen_Jazz.png/220px-Queen_Jazz.png"

