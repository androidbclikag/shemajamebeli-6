package com.example.quiz6

import android.app.Activity
import android.content.Intent
import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    companion object {
        const val REQUEST_CODE = 1
    }
    private var image:ImageModel = ImageModel()
    private var userItems: MutableList<User?> = mutableListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }

    private fun init() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = RecyclerViewAdapter(userItems, image)

    }

    fun addUser(view: View) {
        val intent = Intent(this, AddUserActivity::class.java)
        startActivityForResult(intent, REQUEST_CODE)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (REQUEST_CODE == 1 && resultCode == Activity.RESULT_OK) {
            val itemData = data!!.extras?.getParcelable<User>("addedUser")
            val imageModel = data.extras?.getString("image")
            image.image = imageModel
            userItems.add(itemData)
            recyclerView.adapter!!.notifyItemInserted(0)
            recyclerView.scrollToPosition(0)

        }
    }

}
