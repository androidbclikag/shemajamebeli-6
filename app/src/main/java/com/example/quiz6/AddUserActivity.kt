package com.example.quiz6

import android.app.Activity
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import androidx.room.Room
import kotlinx.android.synthetic.main.activity_add_user.*

class AddUserActivity : AppCompatActivity() {
    private val db by lazy {
        Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "database-name"
        ).build()

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_user)
    }

    fun getRandomImage(): String {
        val imageArray = ImageModel().imageArray
        val imageIndices = imageArray.indices
       val image = imageArray[ imageIndices.random()]
        return image


    }


    fun save(view: View) {
        val title = titleEditText.text.toString()
        val description = descriptionEditText.text.toString()
        if (title.length < 5 || description.length < 30)
            Toast.makeText(
                this,
                "title length should be more than 5 and description- 30",
                Toast.LENGTH_SHORT
            ).show()
        else {
            AsyncTask.execute {

                val user = User()
                user.title = title
                user.description = description
                db.userDao().insertAll(user)
                d("addedUSer", "$user")
                val intentImage = intent.putExtra("image", getRandomImage())
                setResult(Activity.RESULT_OK, intentImage)
                val intent = intent.putExtra("addedUser", user)
                setResult(Activity.RESULT_OK, intent)
                finish()


            }
        }
    }
}
