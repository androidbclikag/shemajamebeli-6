package com.example.quiz6

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.user_item_layout.view.*

class RecyclerViewAdapter(private val dataSet: MutableList<User?>, private val image:ImageModel) :
    RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>() {
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {
            val item = dataSet[adapterPosition]
            itemView.titleTextView.text = item!!.title
            itemView.descriptionTextView.text = item.description
            Glide.with(itemView.context).load(image.image).placeholder(R.mipmap.ic_launcher_round).into(itemView.imageView)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MyViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.user_item_layout, parent, false)
    )

    override fun getItemCount(): Int = dataSet.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.onBind()
    }
}
